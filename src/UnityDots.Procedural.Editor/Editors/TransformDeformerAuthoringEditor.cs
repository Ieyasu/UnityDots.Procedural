using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Procedural.Editors
{
    [CustomEditor(typeof(TransformDeformerAuthoring))]
    public sealed class TransformDeformerAuthoringEditor : UnityEditor.Editor
    {
        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();
            container.AddField(serializedObject, "Translation");
            container.AddField(serializedObject, "Rotation");
            container.AddField(serializedObject, "Scale");
            return container;
        }
    }
}
