using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;

namespace UnityDots.Procedural.Editors
{
    [CustomEditor(typeof(SplineMeshTilingAuthoring))]
    public sealed class SplineMeshTilingAuthoringEditor : UnityEditor.Editor
    {
        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();
            container.AddField(serializedObject, "Mesh");
            container.AddField(serializedObject, "Material");

            CreateTilingGUI(container);
            CreateTransformGUI(container);

            return container;
        }

        private void CreateTilingGUI(VisualElement container)
        {
            container.Add(new Title("Tiling"));
            container.AddField(serializedObject, "Mode");
            container.AddField(serializedObject, "Space");
            container.AddField(serializedObject, "TileCount");
        }

        private void CreateTransformGUI(VisualElement container)
        {
            container.Add(new Title("Transform"));
            container.AddField(serializedObject, "Translation");
            container.AddField(serializedObject, "Rotation");
            container.AddField(serializedObject, "Scale");
        }
    }
}
