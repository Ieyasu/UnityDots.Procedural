using UnityEditor;
using UnityEngine.UIElements;
using UnityDots.Editor;
using UnityDots.Procedural.Authoring;

namespace UnityDots.Procedural.Editors
{
    [CustomEditor(typeof(SplineDeformerAuthoring))]
    public sealed class SplineDeformerAuthoringEditor : UnityEditor.Editor
    {
        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();
            var intervalStart = new SliderField("Interval Start", 0, 1)
            {
                BindingPath = "IntervalStart"
            };
            var intervalEnd = new SliderField("Interval End", 0, 1)
            {
                BindingPath = "IntervalEnd"
            };

            container.AddField(serializedObject, "Spline");
            container.Add(intervalStart);
            container.Add(intervalEnd);

            return container;
        }
    }
}
