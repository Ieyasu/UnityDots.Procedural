using Unity.Jobs;
using Unity.Collections;
using Unity.Entities;
using UnityDots.Collections;
using UnityEngine;

namespace UnityDots.Procedural
{
    [ExecuteAlways]
    [UpdateAfter(typeof(MeshBridgeSystem))]
    public sealed class MeshSourceSystem : JobComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreate()
        {
            var queryDesc = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadOnly<MeshSource>()
                }
            };
            _query = GetEntityQuery(queryDesc);
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            using (var meshSources = NonAllocList<MeshSource>.Get())
            {
                World.DefaultGameObjectInjectionWorld.EntityManager.GetAllUniqueSharedComponentData(meshSources.Data);
                return CreateJobs(meshSources, inputDependencies);
            }
        }

        private JobHandle CreateJobs(NonAllocList<MeshSource> meshSources, JobHandle inputDependencies)
        {
            var meshBridge = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MeshBridgeSystem>();
            var jobHandle = inputDependencies;
            foreach (var meshSource in meshSources.Data)
            {
                if (meshSource.Mesh != null && meshBridge.TryGetMeshSourceData(meshSource.Mesh, out NativeMeshData data))
                {
                    jobHandle = CreateJob(meshSource, data, jobHandle);
                }
            }
            return jobHandle;
        }

        private JobHandle CreateJob(MeshSource meshSource, NativeMeshData meshData, JobHandle inputDependencies)
        {
            _query.SetSharedComponentFilter(meshSource);
            var job = new MeshSourceJob(this, meshData, meshSource.ReadFlags);
            return job.Schedule(_query, inputDependencies);
        }
    }
}
