using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Rendering;
using UnityDots.Collections;
using UnityEngine;

namespace UnityDots.Procedural
{
    // A temporary bridge that converts between managed and unmanaged Meshes.
    [ExecuteAlways]
    public sealed class MeshBridgeSystem : ComponentSystem
    {
        // Source meshes are cached as native data to make it easier to use them in jobs.
        private readonly Dictionary<Mesh, NativeMeshData> _meshSourceData = new Dictionary<Mesh, NativeMeshData>();

        public EntityCommandBuffer CommandBuffer
        {
            get;
            private set;
        }

        public bool TryGetMeshSourceData(Mesh mesh, out NativeMeshData data)
        {
            return _meshSourceData.TryGetValue(mesh, out data);
        }

        public IEnumerable<KeyValuePair<Mesh, NativeMeshData>> GetSourceMeshDataEnumerator()
        {
            foreach (var kvp in _meshSourceData)
            {
                yield return kvp;
            }
        }

        protected override void OnDestroy()
        {
            foreach (var kvp in _meshSourceData)
            {
                kvp.Value.Dispose();
            }
            _meshSourceData.Clear();
        }

        protected override void OnUpdate()
        {
            ReadMeshSources();
            WriteTargetMeshes();
        }

        private void ReadMeshSources()
        {
            using (var sourcesToDelete = NonAllocHashSet<Mesh>.Get())
            using (var meshSources = NonAllocList<MeshSource>.Get())
            {
                foreach (var kvp in _meshSourceData)
                {
                    var mesh = kvp.Key;
                    sourcesToDelete.Data.Add(mesh);
                }

                // Create native mesh data representations.
                World.DefaultGameObjectInjectionWorld.EntityManager.GetAllUniqueSharedComponentData(meshSources.Data);
                foreach (var source in meshSources.Data)
                {
                    if (source.Mesh == null)
                    {
                        continue;
                    }

                    if (!_meshSourceData.TryGetValue(source.Mesh, out NativeMeshData data))
                    {
                        AddMeshSourceData(source.Mesh, source.subMesh);
                    }
                    else if (data.HasValidData)
                    {
                        sourcesToDelete.Data.Remove(source.Mesh);
                    }
                }

                // Remove meshes that were not seen or are invalid.
                foreach (var mesh in sourcesToDelete.Data)
                {
                    RemoveMeshSourceData(mesh);
                }
            }
        }

        private void WriteTargetMeshes()
        {
            var manager = World.DefaultGameObjectInjectionWorld.EntityManager;
            Entities.ForEach((Entity entity, ref MeshData meshData, ref MeshTarget target) =>
            {
                if (!meshData.IsDirty || !manager.Exists(target.Mesh))
                {
                    return;
                }
                meshData.IsDirty = false;

                var mesh = manager.GetSharedComponentData<RenderMesh>(target.Mesh).mesh;
                if (mesh == null)
                {
                    return;
                }

                if (target.WriteFlags.HasFlag(MeshBufferFlags.Index))
                {
                    mesh.Clear();
                }

                if (target.WriteFlags.HasFlag(MeshBufferFlags.Vertex) && manager.HasComponent<MeshVertex>(entity))
                {
                    using (var data = manager.GetBuffer<MeshVertex>(entity).ToNativeArray(Allocator.Temp))
                    {
                        mesh.SetVertices(data);
                    }

                    if (manager.HasComponent<MeshBounds>(entity))
                    {
                        var bounds = manager.GetComponentData<MeshBounds>(entity);
                        manager.SetComponentData(target.Mesh, new RenderBounds { Value = bounds.Value });
                    }
                }

                if (target.WriteFlags.HasFlag(MeshBufferFlags.Normal) && manager.HasComponent<MeshNormal>(entity))
                {
                    using (var data = manager.GetBuffer<MeshNormal>(entity).ToNativeArray(Allocator.Temp))
                    {
                        mesh.SetNormals(data);
                    }
                }

                if (target.WriteFlags.HasFlag(MeshBufferFlags.Tangent) && manager.HasComponent<MeshTangent>(entity))
                {
                    using (var data = manager.GetBuffer<MeshTangent>(entity).ToNativeArray(Allocator.Temp))
                    {
                        mesh.SetTangents(data);
                    }
                }

                if (target.WriteFlags.HasFlag(MeshBufferFlags.UV1) && manager.HasComponent<MeshUV>(entity))
                {
                    using (var data = manager.GetBuffer<MeshUV>(entity).ToNativeArray(Allocator.Temp))
                    {
                        mesh.SetUVs(0, data);
                    }
                }

                if (target.WriteFlags.HasFlag(MeshBufferFlags.Color) && manager.HasComponent<MeshColor>(entity))
                {
                    using (var data = manager.GetBuffer<MeshColor>(entity).ToNativeArray(Allocator.Temp))
                    {
                        mesh.SetColors(data);
                    }
                }

                if (target.WriteFlags.HasFlag(MeshBufferFlags.Index) && manager.HasComponent<MeshIndex>(entity))
                {
                    using (var data = manager.GetBuffer<MeshIndex>(entity).ToNativeArray(Allocator.Temp))
                    {
                        mesh.SetIndices(data, MeshTopology.Triangles, target.SubMesh, false);
                    }
                }

                // Copy mesh bounding box.
                if (manager.HasComponent<MeshBounds>(entity) && manager.HasComponent<RenderBounds>(target.Mesh))
                {
                    var renderBounds = manager.GetComponentData<RenderBounds>(target.Mesh);
                    var meshBounds = manager.GetComponentData<MeshBounds>(entity);
                    renderBounds.Value = meshBounds.Value;
                    manager.SetComponentData(target.Mesh, renderBounds);
                }
            });
        }

        private void AddMeshSourceData(Mesh mesh, int submesh)
        {
            var data = new NativeMeshData(mesh, submesh);
            _meshSourceData.Add(mesh, data);
        }

        private void RemoveMeshSourceData(Mesh mesh)
        {
            if (_meshSourceData.TryGetValue(mesh, out NativeMeshData data))
            {
                data.Dispose();
                _meshSourceData.Remove(mesh);
            }
        }
    }
}
