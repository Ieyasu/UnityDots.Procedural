using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Procedural
{
    [BurstCompile]
    internal struct MeshSourceJob : IJobChunk
    {
        public MeshBufferFlags ReadFlags;
        [ReadOnly] public NativeMeshData MeshData;

        public BufferTypeHandle<MeshVertex> VertexType;
        public BufferTypeHandle<MeshNormal> NormalType;
        public BufferTypeHandle<MeshTangent> TangentType;
        public BufferTypeHandle<MeshUV> UVType;
        public BufferTypeHandle<MeshColor> ColorType;
        public BufferTypeHandle<MeshIndex> IndexType;

        public MeshSourceJob(JobComponentSystem system, NativeMeshData meshData, MeshBufferFlags readFlags)
        {
            MeshData = meshData;
            ReadFlags = readFlags;
            VertexType = system.GetBufferTypeHandle<MeshVertex>();
            NormalType = system.GetBufferTypeHandle<MeshNormal>();
            TangentType = system.GetBufferTypeHandle<MeshTangent>();
            UVType = system.GetBufferTypeHandle<MeshUV>();
            ColorType = system.GetBufferTypeHandle<MeshColor>();
            IndexType = system.GetBufferTypeHandle<MeshIndex>();
        }

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            if (chunk.Has(VertexType) && (ReadFlags & MeshBufferFlags.Vertex) != 0)
            {
                CopyVertices(chunk);
            }

            if (chunk.Has(NormalType) && (ReadFlags & MeshBufferFlags.Normal) != 0)
            {
                CopyNormals(chunk);
            }

            if (chunk.Has(TangentType) && (ReadFlags & MeshBufferFlags.Tangent) != 0)
            {
                CopyTangents(chunk);
            }

            if (chunk.Has(UVType) && (ReadFlags & MeshBufferFlags.UV) != 0)
            {
                CopyUVs(chunk);
            }

            if (chunk.Has(ColorType) && (ReadFlags & MeshBufferFlags.Color) != 0)
            {
                CopyColors(chunk);
            }

            if (chunk.Has(IndexType) && (ReadFlags & MeshBufferFlags.Index) != 0)
            {
                CopyIndices(chunk);
            }
        }

        public void CopyVertices(ArchetypeChunk chunk)
        {
            var verticesAccessor = chunk.GetBufferAccessor(VertexType);
            for (var i = 0; i < chunk.Count; ++i)
            {
                var vertices = verticesAccessor[i];
                vertices.ResizeUninitialized(MeshData.VertexBuffer.Length);
                vertices.Reinterpret<float3>().CopyFrom(MeshData.VertexBuffer);
            }
        }

        public void CopyNormals(ArchetypeChunk chunk)
        {
            var normalsAccessor = chunk.GetBufferAccessor(NormalType);
            for (var i = 0; i < chunk.Count; ++i)
            {
                var normals = normalsAccessor[i];
                normals.ResizeUninitialized(MeshData.NormalBuffer.Length);
                normals.Reinterpret<float3>().CopyFrom(MeshData.NormalBuffer);
            }
        }

        public void CopyTangents(ArchetypeChunk chunk)
        {
            var tangentsAccessor = chunk.GetBufferAccessor(TangentType);
            for (var i = 0; i < chunk.Count; ++i)
            {
                var tangents = tangentsAccessor[i];
                tangents.ResizeUninitialized(MeshData.TangentBuffer.Length);
                tangents.Reinterpret<float4>().CopyFrom(MeshData.TangentBuffer);
            }
        }

        public void CopyUVs(ArchetypeChunk chunk)
        {
            var uvsAccessor = chunk.GetBufferAccessor(UVType);
            for (var i = 0; i < chunk.Count; ++i)
            {
                var uvs = uvsAccessor[i];
                uvs.ResizeUninitialized(MeshData.UVBuffer.Length);
                uvs.Reinterpret<float2>().CopyFrom(MeshData.UVBuffer);
            }
        }

        public void CopyColors(ArchetypeChunk chunk)
        {
            var colorsAccessor = chunk.GetBufferAccessor(ColorType);
            for (var i = 0; i < chunk.Count; ++i)
            {
                var colors = colorsAccessor[i];
                colors.ResizeUninitialized(MeshData.ColorBuffer.Length);
                colors.Reinterpret<float4>().CopyFrom(MeshData.ColorBuffer);
            }
        }

        public void CopyIndices(ArchetypeChunk chunk)
        {
            var indexAccessor = chunk.GetBufferAccessor(IndexType);
            for (var i = 0; i < chunk.Count; ++i)
            {
                var indices = indexAccessor[i];
                indices.ResizeUninitialized(MeshData.IndexBuffer.Length);
                indices.Reinterpret<int>().CopyFrom(MeshData.IndexBuffer);
            }
        }
    }
}
