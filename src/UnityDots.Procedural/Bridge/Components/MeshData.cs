using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Procedural
{
    public struct MeshData : IComponentData
    {
        public bool IsDirty;
    }

    public struct MeshBounds : IComponentData
    {
        public AABB Value;
    }

    [InternalBufferCapacity(0)]
    public struct MeshVertex : IBufferElementData
    {
        public float3 Value;

        public static implicit operator float3(MeshVertex e)
        {
            return e.Value;
        }

        public static implicit operator MeshVertex(float3 e)
        {
            return new MeshVertex { Value = e };
        }
    }

    [InternalBufferCapacity(0)]
    public struct MeshNormal : IBufferElementData
    {
        public float3 Value;

        public static implicit operator float3(MeshNormal e)
        {
            return e.Value;
        }

        public static implicit operator MeshNormal(float3 e)
        {
            return new MeshNormal { Value = e };
        }
    }

    [InternalBufferCapacity(0)]
    public struct MeshTangent : IBufferElementData
    {
        public float4 Value;

        public static implicit operator float4(MeshTangent e)
        {
            return e.Value;
        }

        public static implicit operator MeshTangent(float4 e)
        {
            return new MeshTangent { Value = e };
        }
    }

    [InternalBufferCapacity(0)]
    public struct MeshUV : IBufferElementData
    {
        public float2 Value;

        public static implicit operator float2(MeshUV e)
        {
            return e.Value;
        }

        public static implicit operator MeshUV(float2 e)
        {
            return new MeshUV { Value = e };
        }
    }

    [InternalBufferCapacity(0)]
    public struct MeshColor : IBufferElementData
    {
        public float4 Value;

        public static implicit operator float4(MeshColor e)
        {
            return e.Value;
        }

        public static implicit operator MeshColor(float4 e)
        {
            return new MeshColor { Value = e };
        }
    }

    [InternalBufferCapacity(0)]
    public struct MeshIndex : IBufferElementData
    {
        public int Value;

        public static implicit operator int(MeshIndex e)
        {
            return e.Value;
        }

        public static implicit operator MeshIndex(int e)
        {
            return new MeshIndex { Value = e };
        }
    }
}
