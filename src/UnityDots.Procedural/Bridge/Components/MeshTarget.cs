using System;
using Unity.Entities;

namespace UnityDots.Procedural
{
    [Flags]
    public enum MeshBufferFlags
    {
        None = 0,
        All = ~0,
        Vertex = 1 << 0,
        Normal = 1 << 1,
        Tangent = 1 << 2,
        Color = 1 << 3,
        Index = 1 << 4,
        UV = 1 << 5, // UV is alias for UV1
        UV1 = 1 << 5,
        UV2 = UV1 << 1,
        UV3 = UV2 << 1,
        UV4 = UV3 << 1,
        UV5 = UV4 << 1,
        UV6 = UV5 << 1,
        UV7 = UV6 << 1,
        UV8 = UV7 << 1
    }

    public struct MeshTarget : IComponentData
    {
        public Entity Mesh;
        public MeshBufferFlags WriteFlags;
        public int SubMesh;
    }
}
