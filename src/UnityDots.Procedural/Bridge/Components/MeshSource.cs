using System;
using Unity.Entities;
using UnityEngine;

namespace UnityDots.Procedural
{
    public struct MeshSource : ISharedComponentData, IEquatable<MeshSource>
    {
        public Mesh Mesh;
        public MeshBufferFlags ReadFlags;
        public int subMesh;

        public bool Equals(MeshSource other)
        {
            return Mesh == other.Mesh;
        }

        public override int GetHashCode()
        {
            return (Mesh == null) ? 0 : Mesh.GetHashCode();
        }
    }
}
