using Unity.Entities;

namespace UnityDots.Procedural
{
    public struct SplineDeformer : IComponentData
    {
        public Entity Spline;

        // Interval the mesh is stretched to.
        public float IntervalStart;
        public float IntervalEnd;
    }
}
