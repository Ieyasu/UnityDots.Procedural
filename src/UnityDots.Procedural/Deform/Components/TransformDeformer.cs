using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Procedural
{
    public struct TransformDeformer : IComponentData
    {
        public float3 Translation;
        public float3 Scale;
        public quaternion Rotation;
    }
}
