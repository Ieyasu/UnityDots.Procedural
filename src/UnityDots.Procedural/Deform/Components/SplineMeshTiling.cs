using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Procedural
{
    public enum SplineMeshTilingMode
    {
        Once,
        Repeat,
        Stretch,
        Count
    }

    public enum SplineMeshTilingSpace
    {
        Spline,
        Curve
    }

    public struct SplineMeshTiling : IComponentData
    {
        public int MeshId;
        public int MaterialId;

        public SplineMeshTilingMode Mode;
        public SplineMeshTilingSpace Space;
        public int TileCount;

        // Transform to add to vertices before bending them.
        public float3 Translation;
        public float3 Scale;
        public quaternion Rotation;
    }
}
