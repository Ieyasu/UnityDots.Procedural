using Unity.Entities;
using UnityDots.Splines;

namespace UnityDots.Procedural.Deform
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateAfter(typeof(MeshSourceSystem))]
    [UpdateAfter(typeof(SplineSamplerSystemGroup))]
    public sealed class DeformSystemGroup : ComponentSystemGroup
    {
    }
}
