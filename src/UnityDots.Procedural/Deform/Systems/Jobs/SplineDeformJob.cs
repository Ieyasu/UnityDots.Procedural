using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityDots.Mathematics;
using UnityDots.Splines;

namespace UnityDots.Procedural.Deform
{
    [BurstCompile]
    internal struct SplineDeformJob : IJobChunk
    {
        public ComponentTypeHandle<MeshData> MeshDataType;
        public BufferTypeHandle<MeshVertex> VertexType;
        public BufferTypeHandle<MeshNormal> NormalType;

        [ReadOnly] public ComponentTypeHandle<SplineDeformer> DeformerType;
        [ReadOnly] public ComponentTypeHandle<MeshBounds> MeshBoundsType;
        [ReadOnly] public BufferFromEntity<SplineSample> SplineSampleFromEntity;

        public static EntityQueryDesc GetEntityQueryDesc()
        {
            return new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadWrite<MeshData>(),
                    ComponentType.ReadWrite<MeshVertex>(),
                    ComponentType.ReadOnly<MeshBounds>(),
                    ComponentType.ReadOnly<SplineDeformer>()
                }
            };
        }

        public SplineDeformJob(JobComponentSystem system)
        {
            MeshDataType = system.GetComponentTypeHandle<MeshData>();
            VertexType = system.GetBufferTypeHandle<MeshVertex>();
            NormalType = system.GetBufferTypeHandle<MeshNormal>();

            DeformerType = system.GetComponentTypeHandle<SplineDeformer>(true);
            MeshBoundsType = system.GetComponentTypeHandle<MeshBounds>(true);
            SplineSampleFromEntity = system.GetBufferFromEntity<SplineSample>(true);
        }

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var bounds = chunk.GetNativeArray(MeshBoundsType);
            var deformers = chunk.GetNativeArray(DeformerType);
            var meshData = chunk.GetNativeArray(MeshDataType);
            var verticesAccessor = chunk.GetBufferAccessor(VertexType);
            var normalsAccessor = chunk.GetBufferAccessor(NormalType);
            var hasNormals = chunk.Has(NormalType);

            for (var i = 0; i < chunk.Count; ++i)
            {
                var deformer = deformers[i];
                if (!SplineSampleFromEntity.HasComponent(deformer.Spline))
                {
                    continue;
                }

                var samples = SplineSampleFromEntity[deformer.Spline];
                if (samples.Length == 0)
                {
                    continue;
                }

                var mesh = meshData[i];
                mesh.IsDirty = true;
                meshData[i] = mesh;

                if (!hasNormals)
                {
                    var vertices = verticesAccessor[i];
                    UpdateVertices(deformer, bounds[i], vertices, samples);
                }
                else
                {
                    var vertices = verticesAccessor[i];
                    var normals = normalsAccessor[i];
                    UpdateVerticesAndNormals(deformer, bounds[i], vertices, normals, samples);
                }
            }
        }

        private void UpdateVertices(
                SplineDeformer deformer,
                MeshBounds bounds,
                DynamicBuffer<MeshVertex> vertices,
                DynamicBuffer<SplineSample> samples)
        {
            var meshDepthInv = (deformer.IntervalEnd - deformer.IntervalStart) / bounds.Value.Size.z;
            var meshDepthMin = bounds.Value.Center.z - bounds.Value.Extents.z;
            for (var i = 0; i < vertices.Length; ++i)
            {
                var vertex = vertices[i].Value;
                var t = deformer.IntervalStart + meshDepthInv * (vertex.z - meshDepthMin);
                var sample = SplineSample.Interpolate(t, samples.AsNativeArray());

                // Scale, rotate and translate vertex to follow the path.
                var scale = new float3(sample.Scale.x, sample.Scale.y, 0);
                vertices[i] = Geometry.TransformPoint(vertex, sample.Position, sample.RotationWithRoll, scale);
            }
        }

        private void UpdateVerticesAndNormals(
                SplineDeformer deformer,
                MeshBounds bounds,
                DynamicBuffer<MeshVertex> vertices,
                DynamicBuffer<MeshNormal> normals,
                DynamicBuffer<SplineSample> samples)
        {
            var meshDepthInv = (deformer.IntervalEnd - deformer.IntervalStart) / bounds.Value.Size.z;
            var meshDepthMin = bounds.Value.Center.z - bounds.Value.Extents.z;
            for (var i = 0; i < vertices.Length; ++i)
            {
                var vertex = vertices[i].Value;
                var t = deformer.IntervalStart + meshDepthInv * (vertex.z - meshDepthMin);
                var sample = SplineSample.Interpolate(t, samples.AsNativeArray());

                // Scale, rotate and translate vertex and normal to follow the path.
                var scale = new float3(sample.Scale.x, sample.Scale.y, 0);
                var rotation = sample.RotationWithRoll;
                vertices[i] = Geometry.TransformPoint(vertex, sample.Position, rotation, scale); ;

                if (normals.Length > i)
                {
                    scale.z = 1;
                    normals[i] = math.normalizesafe(Geometry.TransformDirection(normals[i], rotation, scale));
                }
            }
        }
    }
}
