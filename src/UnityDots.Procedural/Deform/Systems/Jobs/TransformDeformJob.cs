using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using UnityDots.Mathematics;

namespace UnityDots.Procedural.Deform
{
    [BurstCompile]
    internal struct TransformDeformJob : IJobChunk
    {
        public ComponentTypeHandle<MeshData> MeshDataType;
        public BufferTypeHandle<MeshVertex> VertexType;
        public BufferTypeHandle<MeshNormal> NormalType;

        [ReadOnly] public ComponentTypeHandle<TransformDeformer> DeformerType;

        public static EntityQueryDesc GetEntityQueryDesc()
        {
            return new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadWrite<MeshData>(),
                    ComponentType.ReadWrite<MeshVertex>(),
                    ComponentType.ReadOnly<TransformDeformer>()
                }
            };
        }

        public TransformDeformJob(JobComponentSystem system)
        {
            MeshDataType = system.GetComponentTypeHandle<MeshData>();
            VertexType = system.GetBufferTypeHandle<MeshVertex>();
            NormalType = system.GetBufferTypeHandle<MeshNormal>();
            DeformerType = system.GetComponentTypeHandle<TransformDeformer>(true);
        }

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var deformers = chunk.GetNativeArray(DeformerType);
            var meshData = chunk.GetNativeArray(MeshDataType);
            var verticesAccessor = chunk.GetBufferAccessor(VertexType);
            var normalsAccessor = chunk.GetBufferAccessor(NormalType);
            var hasNormals = chunk.Has(NormalType);

            for (var i = 0; i < chunk.Count; ++i)
            {
                var mesh = meshData[i];
                mesh.IsDirty = true;
                meshData[i] = mesh;

                UpdateVertices(deformers[i], verticesAccessor[i]);

                if (hasNormals)
                {
                    UpdateNormals(deformers[i], normalsAccessor[i]);
                }
            }
        }

        private void UpdateVertices(TransformDeformer deformer, DynamicBuffer<MeshVertex> vertices)
        {
            for (var i = 0; i < vertices.Length; ++i)
            {
                vertices[i] = Geometry.TransformPoint(vertices[i], deformer.Translation, deformer.Rotation, deformer.Scale);
            }
        }

        private void UpdateNormals(TransformDeformer deformer, DynamicBuffer<MeshNormal> normals)
        {
            for (var i = 0; i < normals.Length; ++i)
            {
                normals[i] = Geometry.TransformDirection(normals[i], deformer.Rotation, deformer.Scale);
            }
        }
    }
}
