using Unity.Jobs;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace UnityDots.Procedural.Deform
{
    [ExecuteAlways]
    [UpdateInGroup(typeof(DeformSystemGroup))]
    [UpdateAfter(typeof(TransformDeformSystem))]
    public sealed class SplineDeformSystem : JobComponentSystem
    {
        private EntityQuery _deformQuery;
        private EntityQuery _boundsQuery;

        protected override void OnCreate()
        {
            _deformQuery = GetEntityQuery(SplineDeformJob.GetEntityQueryDesc());

            var boundsQueryDesc = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    typeof(MeshBounds),
                    ComponentType.ReadOnly<MeshVertex>(),
                    ComponentType.ReadOnly<SplineDeformer>()
                }
            };
            _boundsQuery = GetEntityQuery(boundsQueryDesc);
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            var recalculateBoundsJob = new RecalculateBoundsJob(this);
            var deformJob = new SplineDeformJob(this);
            var recalculateBoundsJobHandle1 = recalculateBoundsJob.Schedule(_boundsQuery, inputDependencies);
            var deformJobHandle = deformJob.Schedule(_deformQuery, recalculateBoundsJobHandle1);
            var recalculateBoundsJobHandle2 = recalculateBoundsJob.Schedule(_boundsQuery, deformJobHandle);
            return recalculateBoundsJobHandle2;
        }
    }
}
