using Unity.Jobs;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace UnityDots.Procedural.Deform
{
    [ExecuteAlways]
    [UpdateInGroup(typeof(DeformSystemGroup))]
    public sealed class TransformDeformSystem : JobComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreate()
        {
            _query = GetEntityQuery(TransformDeformJob.GetEntityQueryDesc());
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            return new TransformDeformJob(this).Schedule(_query, inputDependencies);
        }
    }
}
