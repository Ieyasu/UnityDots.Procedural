using Unity.Collections;
using Unity.Mathematics;
using UnityDots.Collections;
using UnityEngine;

namespace UnityDots.Procedural
{
    public struct NativeMeshData
    {
        public AABB Bounds;
        public MeshTopology Topology;

        public NativeArray<float3> VertexBuffer;
        public NativeArray<float3> NormalBuffer;
        public NativeArray<float4> TangentBuffer;
        public NativeArray<float2> UVBuffer;
        public NativeArray<float4> ColorBuffer;
        public NativeArray<int> IndexBuffer;

        public bool HasValidData => VertexBuffer.IsCreated
                    && NormalBuffer.IsCreated
                    && TangentBuffer.IsCreated
                    && UVBuffer.IsCreated
                    && ColorBuffer.IsCreated
                    && IndexBuffer.IsCreated;

        public NativeMeshData(Mesh mesh, int submesh = 0, Allocator allocator = Allocator.Persistent)
        {
            Bounds = new AABB
            {
                Center = mesh.bounds.center,
                Extents = mesh.bounds.extents
            };
            Topology = MeshTopology.Triangles;

            using (var buffer = NonAllocList<Vector3>.Get())
            {
                mesh.GetVertices(buffer.Data);
                VertexBuffer = new NativeArray<float3>(buffer.Count, allocator, NativeArrayOptions.UninitializedMemory);
                VertexBuffer.CopyFrom(buffer.Data);
            }

            using (var buffer = NonAllocList<Vector3>.Get())
            {
                mesh.GetNormals(buffer.Data);
                NormalBuffer = new NativeArray<float3>(buffer.Count, allocator, NativeArrayOptions.UninitializedMemory);
                NormalBuffer.CopyFrom(buffer.Data);
            }

            using (var buffer = NonAllocList<Vector4>.Get())
            {
                mesh.GetTangents(buffer.Data);
                TangentBuffer = new NativeArray<float4>(buffer.Count, allocator, NativeArrayOptions.UninitializedMemory);
                TangentBuffer.CopyFrom(buffer.Data);
            }

            using (var buffer = NonAllocList<Vector2>.Get())
            {
                mesh.GetUVs(0, buffer.Data);
                UVBuffer = new NativeArray<float2>(buffer.Count, allocator, NativeArrayOptions.UninitializedMemory);
                UVBuffer.CopyFrom(buffer.Data);
            }

            using (var buffer = NonAllocList<Color>.Get())
            {
                mesh.GetColors(buffer.Data);
                ColorBuffer = new NativeArray<float4>(buffer.Count, allocator, NativeArrayOptions.UninitializedMemory);
                ColorBuffer.CopyFrom(buffer.Data);
            }

            using (var buffer = NonAllocList<int>.Get())
            {
                mesh.GetIndices(buffer.Data, submesh);
                IndexBuffer = new NativeArray<int>(buffer.Count, allocator, NativeArrayOptions.UninitializedMemory);
                IndexBuffer.CopyFrom(buffer.Data);
            }
        }

        public void CopyTo(Mesh mesh)
        {
            var changeTopology = IndexBuffer.IsCreated;
            if (changeTopology)
            {
                mesh.Clear();
            }

            if (VertexBuffer.IsCreated)
            {
                mesh.SetVertices(VertexBuffer);
            }
            if (NormalBuffer.IsCreated)
            {
                mesh.SetNormals(NormalBuffer);
            }
            if (TangentBuffer.IsCreated)
            {
                mesh.SetTangents(TangentBuffer);
            }
            if (UVBuffer.IsCreated)
            {
                mesh.SetUVs(0, UVBuffer);
            }
            if (ColorBuffer.IsCreated)
            {
                mesh.SetColors(ColorBuffer);
            }
            if (IndexBuffer.IsCreated)
            {
                mesh.SetIndices(IndexBuffer, Topology, 0, false);
            }
            mesh.bounds = new Bounds(Bounds.Center, Bounds.Size);
        }

        public void Dispose()
        {
            if (VertexBuffer.IsCreated)
            {
                VertexBuffer.Dispose();
            }
            if (NormalBuffer.IsCreated)
            {
                NormalBuffer.Dispose();
            }
            if (TangentBuffer.IsCreated)
            {
                TangentBuffer.Dispose();
            }
            if (UVBuffer.IsCreated)
            {
                UVBuffer.Dispose();
            }
            if (ColorBuffer.IsCreated)
            {
                ColorBuffer.Dispose();
            }
            if (IndexBuffer.IsCreated)
            {
                IndexBuffer.Dispose();
            }
        }
    }
}
