using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Procedural
{
    [BurstCompile]
    internal struct RecalculateBoundsJob : IJobChunk
    {
        public ComponentTypeHandle<MeshBounds> BoundsType;

        [ReadOnly] public BufferTypeHandle<MeshVertex> VertexType;

        public static EntityQueryDesc GetQueryDesc()
        {
            return new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadWrite<MeshBounds>(),
                    ComponentType.ReadOnly<MeshVertex>()
                }
            };
        }

        public RecalculateBoundsJob(JobComponentSystem system)
        {
            BoundsType = system.GetComponentTypeHandle<MeshBounds>();
            VertexType = system.GetBufferTypeHandle<MeshVertex>();
        }

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var bounds = chunk.GetNativeArray(BoundsType);
            var verticesAccessor = chunk.GetBufferAccessor(VertexType);

            for (var i = 0; i < chunk.Count; ++i)
            {
                bounds[i] = new MeshBounds { Value = CalculateBounds(verticesAccessor[i]) };
            }
        }

        private AABB CalculateBounds(DynamicBuffer<MeshVertex> vertices)
        {
            if (vertices.Length == 0)
            {
                return new AABB
                {
                    Center = float3.zero,
                    Extents = float3.zero
                };
            }

            var min = vertices[0].Value;
            var max = vertices[0].Value;
            for (var i = 0; i < vertices.Length; ++i)
            {
                min = math.min(vertices[i], min);
                max = math.max(vertices[i], max);
            }

            return new AABB
            {
                Center = 0.5f * (max + min),
                Extents = 0.5f * (max - min)
            };
        }
    }
}
