using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Procedural
{
    [BurstCompile]
    public struct RecalculateTangentsJob : IJobChunk
    {
        [WriteOnly] public BufferTypeHandle<MeshTangent> TangentType;

        [ReadOnly] public BufferTypeHandle<MeshVertex> VertexType;
        [ReadOnly] public BufferTypeHandle<MeshNormal> NormalType;
        [ReadOnly] public BufferTypeHandle<MeshUV> UVType;
        [ReadOnly] public BufferTypeHandle<MeshIndex> IndexType;

        public static EntityQueryDesc GetQueryDesc()
        {
            return new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadWrite<MeshTangent>(),
                    ComponentType.ReadOnly<MeshVertex>(),
                    ComponentType.ReadOnly<MeshNormal>(),
                    ComponentType.ReadOnly<MeshUV>(),
                    ComponentType.ReadOnly<MeshIndex>()
                }
            };
        }

        public RecalculateTangentsJob(JobComponentSystem system)
        {
            TangentType = system.GetBufferTypeHandle<MeshTangent>();
            VertexType = system.GetBufferTypeHandle<MeshVertex>(true);
            NormalType = system.GetBufferTypeHandle<MeshNormal>(true);
            UVType = system.GetBufferTypeHandle<MeshUV>(true);
            IndexType = system.GetBufferTypeHandle<MeshIndex>(true);
        }

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var vertexAccessor = chunk.GetBufferAccessor(VertexType);
            var normalAccessor = chunk.GetBufferAccessor(NormalType);
            var uvAccessor = chunk.GetBufferAccessor(UVType);
            var indexAccessor = chunk.GetBufferAccessor(IndexType);
            var tangentAccessor = chunk.GetBufferAccessor(TangentType);

            var tan1 = new NativeList<float3>(Allocator.Temp);
            var tan2 = new NativeList<float3>(Allocator.Temp);

            for (var c = 0; c < chunk.Count; ++c)
            {
                var vertices = vertexAccessor[c];
                var normals = normalAccessor[c];
                var uvs = uvAccessor[c];
                var indices = indexAccessor[c];
                var tangents = tangentAccessor[c];

                if (vertices.Length == 0 || normals.Length != vertices.Length || uvs.Length != vertices.Length)
                {
                    return;
                }

                tangents.ResizeUninitialized(vertices.Length);
                tan1.ResizeUninitialized(vertices.Length);
                tan2.ResizeUninitialized(vertices.Length);

                var triangleCount = indices.Length / 3;
                for (var i = 0; i < triangleCount; ++i)
                {
                    var i1 = indices[3 * i];
                    var i2 = indices[3 * i + 1];
                    var i3 = indices[3 * i + 2];

                    var v1 = vertices[i1].Value;
                    var v2 = vertices[i2].Value;
                    var v3 = vertices[i3].Value;

                    var w1 = uvs[i1].Value;
                    var w2 = uvs[i2].Value;
                    var w3 = uvs[i3].Value;

                    var d1 = v2 - v1;
                    var d2 = v3 - v1;
                    var s1 = w2 - w1;
                    var s2 = w3 - w1;

                    var r = 1.0f / (s1.x * -s2.x * s1.y);
                    var sdir = r * new float3(
                            (s2.y * d1.x) - (s1.y * d2.x),
                            (s2.y * d1.y) - (s1.y * d2.y),
                            (s2.y * d1.z) - (s1.y * d2.z));
                    var tdir = r * new float3(
                            (s1.x * d2.x) - (s2.x * d1.x),
                            (s1.x * d2.y) - (s2.x * d1.y),
                            (s1.x * d2.z) - (s2.x * d1.z));

                    tan1[i1] += sdir;
                    tan1[i2] += sdir;
                    tan1[i3] += sdir;

                    tan2[i1] += tdir;
                    tan2[i2] += tdir;
                    tan2[i3] += tdir;
                }

                for (var i = 0; i < vertices.Length; ++i)
                {
                    var n = normals[i];
                    var t = tan1[i];

                    var binormal = math.cross(n, t);
                    t = math.cross(binormal, n);

                    // Calculate handedness
                    var w = (math.dot(math.cross(n, t), tan2[i]) < 0.0f) ? -1.0f : 1.0f;
                    tangents[i] = new float4(t.x, t.y, t.z, w);
                }
            }

            tan1.Dispose();
            tan2.Dispose();
        }
    }
}
