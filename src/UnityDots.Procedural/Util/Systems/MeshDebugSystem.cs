using Unity.Entities;
using UnityEngine;

namespace UnityDots.Procedural
{
    [ExecuteAlways]
    public sealed class MeshDebugSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithAllReadOnly(typeof(MeshDebugNormalsTag)).ForEach((DynamicBuffer<MeshVertex> vertices, DynamicBuffer<MeshNormal> normals) =>
            {
                for (var i = 0; i < normals.Length - 1; ++i)
                {
                    UnityEngine.Debug.DrawRay(vertices[i].Value, 0.2f * normals[i].Value, UnityEngine.Color.magenta);
                }
            });
        }
    }
}
