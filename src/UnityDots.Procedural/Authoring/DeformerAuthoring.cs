using Unity.Entities;
using UnityEngine;

namespace UnityDots.Procedural
{
    public abstract class DeformerAuthoring : MonoBehaviour
    {
        protected void AddMeshSource(Entity entity, EntityManager manager, MeshBufferFlags flags)
        {
            if (manager.HasComponent<MeshSource>(entity))
            {
                var data = manager.GetSharedComponentData<MeshSource>(entity);
                data.ReadFlags |= flags;
                manager.SetSharedComponentData(entity, data);
                return;
            }

            var meshFilter = GetComponent<MeshFilter>();
            manager.AddSharedComponentData(entity, new MeshSource
            {
                Mesh = meshFilter.sharedMesh,
                ReadFlags = flags
            });
            meshFilter.sharedMesh = Instantiate(meshFilter.sharedMesh);
        }

        protected void AddMeshTarget(Entity entity, EntityManager manager, MeshBufferFlags flags)
        {
            if (manager.HasComponent<MeshTarget>(entity))
            {
                var data = manager.GetComponentData<MeshTarget>(entity);
                data.WriteFlags |= flags;
                manager.SetComponentData(entity, data);
                return;
            }

            manager.AddComponentData(entity, new MeshTarget
            {
                Mesh = entity,
                WriteFlags = flags
            });
        }

        protected void AddMeshData(Entity entity, EntityManager manager, MeshBufferFlags flags)
        {
            if (!manager.HasComponent<MeshData>(entity))
            {
                manager.AddComponent<MeshData>(entity);
            }
            if (!manager.HasComponent<MeshBounds>(entity))
            {
                manager.AddComponent<MeshBounds>(entity);
            }

            if (flags.HasFlag(MeshBufferFlags.Vertex) && !manager.HasComponent<MeshVertex>(entity))
            {
                manager.AddBuffer<MeshVertex>(entity);
            }
            if (flags.HasFlag(MeshBufferFlags.Normal) && !manager.HasComponent<MeshNormal>(entity))
            {
                manager.AddBuffer<MeshNormal>(entity);
            }
            if (flags.HasFlag(MeshBufferFlags.Tangent) && !manager.HasComponent<MeshTangent>(entity))
            {
                manager.AddBuffer<MeshTangent>(entity);
            }
            if (flags.HasFlag(MeshBufferFlags.UV) && !manager.HasComponent<MeshUV>(entity))
            {
                manager.AddBuffer<MeshUV>(entity);
            }
            if (flags.HasFlag(MeshBufferFlags.Color) && !manager.HasComponent<MeshColor>(entity))
            {
                manager.AddBuffer<MeshColor>(entity);
            }
            if (flags.HasFlag(MeshBufferFlags.Index) && !manager.HasComponent<MeshIndex>(entity))
            {
                manager.AddBuffer<MeshIndex>(entity);
            }
        }
    }
}
