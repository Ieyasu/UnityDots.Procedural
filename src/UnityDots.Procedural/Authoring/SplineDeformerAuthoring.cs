using System.Collections.Generic;
using Unity.Entities;
using Unity.Rendering;
using UnityDots.Splines.Authoring;
using UnityEngine;

namespace UnityDots.Procedural.Authoring
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public sealed class SplineDeformerAuthoring : DeformerAuthoring, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
    {
        public SplineAuthoring Spline;
        public float IntervalStart = 0;
        public float IntervalEnd = 1;

        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            referencedPrefabs.Add(Spline.gameObject);
        }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            // We want to read and write from Vertex and Normal buffers.
            var flags = MeshBufferFlags.Normal | MeshBufferFlags.Vertex;
            AddMeshSource(entity, dstManager, flags);
            AddMeshTarget(entity, dstManager, flags);
            AddMeshData(entity, dstManager, flags);

            dstManager.AddComponentData(entity, new SplineDeformer
            {
                Spline = conversionSystem.GetPrimaryEntity(Spline.gameObject),
                IntervalStart = IntervalStart,
                IntervalEnd = IntervalEnd,
            });

            // If we flip the direction of the vertices, we also need to render them flipped.
            if (IntervalStart > IntervalEnd)
            {
                dstManager.AddComponent<RenderMeshFlippedWindingTag>(entity);
            }
        }
    }
}
