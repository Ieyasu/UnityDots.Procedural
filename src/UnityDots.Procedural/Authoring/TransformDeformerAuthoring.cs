using Unity.Entities;
using UnityEngine;

namespace UnityDots.Procedural
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public sealed class TransformDeformerAuthoring : DeformerAuthoring, IConvertGameObjectToEntity
    {
        public Vector3 Translation;
        public Vector3 Rotation;
        public Vector3 Scale = Vector3.one;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            // We want to read and write from Vertex and Normal buffers.
            var flags = MeshBufferFlags.Normal | MeshBufferFlags.Vertex;
            AddMeshSource(entity, dstManager, flags);
            AddMeshTarget(entity, dstManager, flags);
            AddMeshData(entity, dstManager, flags);

            dstManager.AddComponentData(entity, new TransformDeformer
            {
                Translation = Translation,
                Rotation = Quaternion.Euler(Rotation),
                Scale = Scale
            });
        }
    }
}
