using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Procedural
{
    [RequireComponent(typeof(MeshFilter))]
    public sealed class SplineMeshTilingAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public Mesh Mesh;
        public Material Material;

        public SplineMeshTilingMode Mode = SplineMeshTilingMode.Repeat;
        public SplineMeshTilingSpace Space = SplineMeshTilingSpace.Curve;
        public int TileCount = 1;

        public Vector3 Translation;
        public Vector3 Rotation;
        public Vector3 Scale = Vector3.one;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new SplineMeshTiling
            {
                MeshId = Mesh.GetInstanceID(),
                MaterialId = Material.GetInstanceID(),
                Space = Space,
                Mode = Mode,
                TileCount = (Mode == SplineMeshTilingMode.Count) ? TileCount : 0,

                Translation = Translation,
                Rotation = quaternion.Euler(Rotation),
                Scale = Scale
            });
        }
    }
}
